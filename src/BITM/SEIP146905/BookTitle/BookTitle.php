<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;
    public function __construct()
    {
        parent:: __construct();

    }
    public function setData($postVariable=null)
    {

       if(array_key_exists("id",$postVariable))
       {
        $this->id =        $postVariable['id'];
       }
        if(array_key_exists("book_title",$postVariable))
        {
            $this->book_title =        $postVariable['book_title'];
        }
        if(array_key_exists("author_name",$postVariable))
        {
            $this->author_name =        $postVariable['author_name'];
        }
    }
    public function store(){
        $arrayData=array($this->book_title,$this->author_name);
        $sql="insert into book_title(book_title,author_name)VALUES (?,?)";
       $STH= $this->conn->prepare($sql);
       $result= $STH->execute($arrayData);
        if($result)
        Message::Message("data has been inserted successfully");
        else
            Message::Message("Failure ....Data is not inserted");
        Utility::redirect('create.php');
    }
}
?>