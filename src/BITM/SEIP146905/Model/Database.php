<?php
namespace App\Model;
use PDO;
use PDOException;

class Database {
    public $conn;
    public $dbname="atomic_project_b36";
    public $host="localhost";
    public $username="root";
    public $password="";
   public function __construct()
    {
        try
        {
            $this->conn=new PDO("mysql:host=localhost;dbname=atomic_project_b36", $this->username,$this->password);
       // echo "connection successfuly";
        }
        catch(PDOException $e)
        {
             $e->getMessage();
        }
    }
}